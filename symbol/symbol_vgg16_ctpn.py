import mxnet as mx
from common import conv_act_layer
from bi_lstm import bi_lstm_unroll

def get_symbol_train(num_classes=20, nms_thresh=0.3, force_suppress=False, nms_topk=2000, batch_size=1):
    """
    Single-shot multi-box detection with VGG 16 layers ConvNet
    This is a modified version, with fc6/fc7 layers replaced by conv layers
    And the network is slightly smaller than original VGG 16 network
    This is a training network with losses

    Parameters:
    ----------
    num_classes: int
        number of object classes not including background
    nms_thresh : float
        non-maximum suppression threshold
    force_suppress : boolean
        whether suppress different class objects
    nms_topk : int
        apply NMS to top K detections

    Returns:
    ----------
    mx.Symbol
    """
    data = mx.symbol.Variable(name="data")
    label = mx.symbol.Variable(name="label")

    # group 1
    conv1_1 = mx.symbol.Convolution(
        data=data, kernel=(3, 3), pad=(1, 1), num_filter=64, name="conv1_1")
    relu1_1 = mx.symbol.Activation(data=conv1_1, act_type="relu", name="relu1_1")
    conv1_2 = mx.symbol.Convolution(
        data=relu1_1, kernel=(3, 3), pad=(1, 1), num_filter=64, name="conv1_2")
    relu1_2 = mx.symbol.Activation(data=conv1_2, act_type="relu", name="relu1_2")
    pool1 = mx.symbol.Pooling(
        data=relu1_2, pool_type="max", kernel=(2, 2), stride=(2, 2), name="pool1")
    # group 2
    conv2_1 = mx.symbol.Convolution(
        data=pool1, kernel=(3, 3), pad=(1, 1), num_filter=128, name="conv2_1")
    relu2_1 = mx.symbol.Activation(data=conv2_1, act_type="relu", name="relu2_1")
    conv2_2 = mx.symbol.Convolution(
        data=relu2_1, kernel=(3, 3), pad=(1, 1), num_filter=128, name="conv2_2")
    relu2_2 = mx.symbol.Activation(data=conv2_2, act_type="relu", name="relu2_2")
    pool2 = mx.symbol.Pooling(
        data=relu2_2, pool_type="max", kernel=(2, 2), stride=(2, 2), name="pool2")
    # group 3
    conv3_1 = mx.symbol.Convolution(
        data=pool2, kernel=(3, 3), pad=(1, 1), num_filter=256, name="conv3_1")
    relu3_1 = mx.symbol.Activation(data=conv3_1, act_type="relu", name="relu3_1")
    conv3_2 = mx.symbol.Convolution(
        data=relu3_1, kernel=(3, 3), pad=(1, 1), num_filter=256, name="conv3_2")
    relu3_2 = mx.symbol.Activation(data=conv3_2, act_type="relu", name="relu3_2")
    conv3_3 = mx.symbol.Convolution(
        data=relu3_2, kernel=(3, 3), pad=(1, 1), num_filter=256, name="conv3_3")
    relu3_3 = mx.symbol.Activation(data=conv3_3, act_type="relu", name="relu3_3")
    pool3 = mx.symbol.Pooling(
        data=relu3_3, pool_type="max", kernel=(2, 2), stride=(2, 2), \
        pooling_convention="full", name="pool3")
    # group 4
    conv4_1 = mx.symbol.Convolution(
        data=pool3, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv4_1")
    relu4_1 = mx.symbol.Activation(data=conv4_1, act_type="relu", name="relu4_1")
    conv4_2 = mx.symbol.Convolution(
        data=relu4_1, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv4_2")
    relu4_2 = mx.symbol.Activation(data=conv4_2, act_type="relu", name="relu4_2")
    conv4_3 = mx.symbol.Convolution(
        data=relu4_2, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv4_3")
    relu4_3 = mx.symbol.Activation(data=conv4_3, act_type="relu", name="relu4_3")
    pool4 = mx.symbol.Pooling(
        data=relu4_3, pool_type="max", kernel=(2, 2), stride=(2, 2), name="pool4")
    # group 5
    conv5_1 = mx.symbol.Convolution(
        data=pool4, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv5_1")
    relu5_1 = mx.symbol.Activation(data=conv5_1, act_type="relu", name="relu5_1")
    conv5_2 = mx.symbol.Convolution(
        data=relu5_1, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv5_2")
    relu5_2 = mx.symbol.Activation(data=conv5_2, act_type="relu", name="relu5_2")
    conv5_3 = mx.symbol.Convolution(
        data=relu5_2, kernel=(3, 3), pad=(1, 1), num_filter=512, name="conv5_3")
    relu5_3 = mx.symbol.Activation(data=conv5_3, act_type="relu", name="relu5_3")
    pool5 = mx.symbol.Pooling(
        data=relu5_3, pool_type="max", kernel=(3, 3), stride=(1, 1),
        pad=(1,1), name="pool5")

    # start ctpn layers.
    #im2col = mx.symbol.Conv_im2col(data=conv5_3, kernel=(3, 3), pad=(1, 1), num_filter=512, name="im2col")
    im2col = mx.symbol.CaffeOp(data_0=conv5_3, num_data=1, num_weight=0, name='im2col',
            prototxt='layer {type: \"Im2col\" convolution_param { pad: 1 kernel_size: 3 stride: 1}}')

    #arg_shapes, out_shapes, aux_shapes = im2col.infer_shape(data=(1, 3, 1440, 960))
    im2col_transpose = mx.symbol.transpose(im2col, axes=(3,2,0,1))
    lstm_input = mx.symbol.reshape(im2col_transpose, shape=(0, 0, -1))
    lstm_input_ = mx.symbol.transpose(lstm_input, axes=(1, 0, 2))
    arg_shapes, out_shapes, aux_shapes = lstm_input_.infer_shape(data=(batch_size, 3, 480, 720))
    lstm_output_fc = bi_lstm_unroll(2, out_shapes,
                           num_classes = num_classes,
                           input_sym = lstm_input_,
                           batch_size = 1)
    #lstm_output_fc = relu5_3

    # .infer_shape(data=(batch_size, 3, 960, 1440),l0_init_h=(batch_size, 60, 128),l2_init_h=(batch_size, 60, 128),l1_init_h=(batch_size, 60, 128), l3_init_h=(batch_size, 60, 128), l0_init_c=(batch_size, 60, 128),l2_init_c=(batch_size, 60, 128),l1_init_c=(batch_size, 60, 128),l3_init_c=(batch_size, 60, 128))

    # ctpn cls relative layers
    rpn_cls_score_multilabel = mx.symbol.Convolution(
        data=lstm_output_fc, kernel=(3, 3), pad=(1, 1), num_filter=(num_classes+1)*10, name="rpn_cls_score_multilabel")
    rpn_cls_score_multilabel_reshape = mx.symbol.Reshape(data=rpn_cls_score_multilabel, shape=(0, 0, -1))
    cls_preds_ = mx.symbol.transpose(rpn_cls_score_multilabel_reshape, axes=(0,2,1))
    cls_preds__ = mx.symbol.Reshape(data=cls_preds_, shape=(0, 0, 10, -1))
    cls_preds___ = mx.symbol.transpose(cls_preds__, axes=(0, 3, 1, 2))
    cls_preds = mx.symbol.Reshape(data=cls_preds___, shape=(0, 0, -1), name="rpn_cls_preds")
    #rpn_cls_score_perm = mx.symbol.transpose(rpn_cls_score_multilabel, axes=(0,2,3,1))
    #rpn_cls_score_flat = mx.symbol.Flatten(data=rpn_cls_score_perm, name='rpn_cls_score_flat')

    # ctpn bbox relative layers
    rpn_bbox_pred = mx.symbol.Convolution(
        data=lstm_output_fc, kernel=(3, 3), pad=(1, 1), num_filter=20, name="rpn_bbox_pred")
    rpn_bbox_pred_perm = mx.symbol.transpose(rpn_bbox_pred, axes=(0,2,3,1))
    rpn_bbox_pred_flat = mx.symbol.Flatten(data=rpn_bbox_pred_perm, name='rpn_bbox_pred_flat')

    # ctpn prior boxes
    size_str = '(16)'
    ratio_str = '(0.5, 0.707107, 1, 1.41421, 2, 2.82843, 4, 5.65685, 8, 11.3137)'
    clip = True
    step = '(-1.0, -1.0)'
    anchors = mx.contrib.symbol.CtpnPrior(lstm_output_fc, sizes=size_str, ratios=ratio_str, \
        clip=clip, steps=step)
    '''
    arg_shapes, out_shapes, aux_shapes = lstm_output_fc.infer_shape(data=(batch_size, 3, 960, 1440),
                                                                    l0_init_h=(batch_size, 60, 128),
                                                                    l2_init_h=(batch_size, 60, 128),
                                                                    l1_init_h=(batch_size, 60, 128),
                                                                    l3_init_h=(batch_size, 60, 128),
                                                                    l0_init_c=(batch_size, 60, 128),
                                                                    l2_init_c=(batch_size, 60, 128),
                                                                    l1_init_c=(batch_size, 60, 128),
                                                                    l3_init_c=(batch_size, 60, 128))
    '''
    anchors = mx.symbol.Flatten(data=anchors)
    anchor_boxes = mx.symbol.Reshape(data=anchors, shape=(0, -1, 4), name="ctpn_anchors")

    tmp = mx.contrib.symbol.CtpnTarget(
        *[anchor_boxes, label, cls_preds], overlap_threshold=.6, \
        ignore_label=-1, negative_mining_ratio=1, minimum_negative_samples=0, \
        negative_mining_thresh=.5, variances=(0.1, 0.1, 0.2, 0.2), name="tmp")
    loc_target = tmp[0]
    loc_target_mask = tmp[1]
    cls_target = tmp[2]

    #loc_target_ = loc_target
    #loc_target_trans_ = mx.symbol.Flatten(data=loc_target_, name="loc_target")

    cls_prob = mx.symbol.SoftmaxOutput(data=cls_preds, label=cls_target, \
        ignore_label=-1, use_ignore=True, grad_scale=1., multi_output=True, \
        normalization='valid', name="cls_prob")
    loc_loss_ = mx.symbol.smooth_l1(name="loc_loss_", \
        data=loc_target_mask * (rpn_bbox_pred_flat - loc_target), scalar=1.0)
    loc_loss = mx.symbol.MakeLoss(loc_loss_, grad_scale=1., \
        normalization='valid', name="loc_loss")

    # monitoring training status
    cls_label = mx.symbol.MakeLoss(data=cls_target, grad_scale=0, name="cls_label")
    
    det = mx.contrib.symbol.CtpnDetection(*[cls_prob, rpn_bbox_pred_flat, anchor_boxes], \
        name="detection", threshold=0.2, nms_threshold=nms_thresh, force_suppress=force_suppress,
        variances=(0.1, 0.1, 0.2, 0.2), nms_topk=nms_topk)
    det = mx.symbol.MakeLoss(data=det, grad_scale=0, name="det_out")

    
    #out = mx.symbol.Group([cls_prob, loc_loss, cls_label, loc_target_trans_])
    out = mx.symbol.Group([cls_prob, loc_loss, cls_label, det])

    return out

def get_symbol(num_classes=20, nms_thresh=0.5, force_suppress=False, nms_topk=2000):
    """
    Single-shot multi-box detection with VGG 16 layers ConvNet
    This is a modified version, with fc6/fc7 layers replaced by conv layers
    And the network is slightly smaller than original VGG 16 network
    This is the detection network

    Parameters:
    ----------
    num_classes: int
        number of object classes not including background
    nms_thresh : float
        threshold of overlap for non-maximum suppression
    force_suppress : boolean
        whether suppress different class objects
    nms_topk : int
        apply NMS to top K detections

    Returns:
    ----------
    mx.Symbol
    """
    net = get_symbol_train(num_classes)
    cls_preds = net.get_internals()["rpn_cls_preds_output"]

    loc_preds = net.get_internals()["rpn_bbox_pred_flat_output"]
    #loc_target_trans_ = net.get_internals()["loc_target_output"]

    anchor_boxes = net.get_internals()["ctpn_anchors_output"]

    cls_prob = mx.symbol.SoftmaxActivation(data=cls_preds, mode='channel', \
        name='cls_prob')
    out = mx.contrib.symbol.CtpnDetection(*[cls_prob, loc_preds, anchor_boxes], \
        name="detection", threshold=0.2, nms_threshold=nms_thresh, force_suppress=force_suppress,
        variances=(0.1, 0.1, 0.2, 0.2), nms_topk=nms_topk)
    return out
