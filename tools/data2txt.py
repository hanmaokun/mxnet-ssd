# -*- coding: utf-8 -*-

import os, sys
import exifread
import fnmatch
import xml.etree.cElementTree as ET
import cv2
import numpy as np
import argparse
import random

if __name__ == '__main__':
	reload(sys)
	sys.setdefaultencoding('utf-8')

	parser = argparse.ArgumentParser(description="Generate txt summary file from data dir.")


	parser.add_argument("--src", default="", help="source annotated image directory.")
	parser.add_argument("--type", default="train", help="train, val, or all")

	args = parser.parse_args()

	custom_src_dir = args.src

	f_train_file = os.path.join(custom_src_dir, 'trainval.txt')
	if os.path.exists(f_train_file):
		os.remove(f_train_file)
	f_train = open(f_train_file, 'w')

	f_test_file = os.path.join(custom_src_dir, 'test.txt')
	if os.path.exists(f_test_file):
		os.remove(f_test_file)
	f_test = open(f_test_file, 'w')

	r_range =[7,7]
	for root, dir_names, file_names in os.walk(custom_src_dir):
		for xml_file_name in fnmatch.filter(file_names, '*.xml'):
			RDM = random.randint(1, 10)

			xml_file_path = os.path.join(root, xml_file_name) 
			tree = ET.ElementTree(file=xml_file_path)
			tree_root = tree.getroot()
			sizes = tree_root.findall('size')
			objs = tree_root.findall('object')
			paths = tree_root.findall('path')
			for path in paths:
				suffix = os.path.splitext(path.text)[1]

			suffix = '.jpg'
			img_name = xml_file_path[:-4] + suffix

			if RDM in r_range:
				f_test.write(img_name + '\t' + xml_file_path + '\n')
			else:
				f_train.write(img_name + '\t' + xml_file_path + '\n')

	f_train.close()
	f_test.close()
