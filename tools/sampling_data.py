import os, sys
import exifread
import fnmatch
import xml.etree.cElementTree as ET
import cv2
import numpy as np
import argparse
import random
from shutil import copyfile

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Sub sampling data from origin dir.")

	parser.add_argument("--src", default="", help="source annotated image directory.")
	parser.add_argument("--dst", default="", help="destination annotated image directory.")

	args = parser.parse_args()

	custom_src_dir = args.src
	dest_dir = args.dst
	
	r_range =[7,7]

	for root, dir_names, file_names in os.walk(custom_src_dir):
		for xml_file_name in fnmatch.filter(file_names, '*.xml'):
			RDM = random.randint(1, 10)

			if RDM in r_range:
				img_file_name = xml_file_name[:-4] + '.jpg'

				xml_file_path = os.path.join(custom_src_dir, xml_file_name)
				img_file_path = os.path.join(custom_src_dir, img_file_name)

				copyfile(xml_file_path, os.path.join(dest_dir, xml_file_name))
				copyfile(img_file_path, os.path.join(dest_dir, img_file_name))