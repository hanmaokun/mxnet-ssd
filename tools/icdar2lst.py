import os, sys, argparse, fnmatch, xml.etree.cElementTree as ET 
import cv2

LABELS = ['BianHao', 'RiQi', 'YiYuan', 'JinE_1']

if __name__ == "__main__":
	reload(sys)
	sys.setdefaultencoding("utf-8")

	parser = argparse.ArgumentParser()
	parser.add_argument("--src")
	parser.add_argument("--txt")
	args = parser.parse_args()
	SRC_DIR = args.src
	TXT_FILE = args.txt

	txt_file = open(TXT_FILE, 'r')
	output_file_name = os.path.join(SRC_DIR, os.path.split(TXT_FILE)[1].split('.')[0] + '.lst')
	if os.path.exists(output_file_name):
		os.remove(output_file_name)
	output_file = open(output_file_name, 'w')

	pairs = txt_file.readlines()

	anno_ctr = 0
	for pair in pairs:
		anno_str = str(anno_ctr)
		anno_str += '\t2\t6\t'
		imgNgt = pair.split(' ')
		img_path_to_record = imgNgt[0]
		gt_path_full = os.path.join(SRC_DIR, 'gt_' + imgNgt[1][:-1])
		
		img_path_full = os.path.join(SRC_DIR, img_path_to_record)

		img_data = cv2.imread(img_path_full)
		img_width_real = img_data.shape[1]
		img_height_real = img_data.shape[0]

		gt_file = open(gt_path_full, 'r')

		annotations = gt_file.readlines()

		label_content_str = anno_str
		valid_roi_found = False
		for anno in annotations:
			anno_arr = anno.split(' ')
			valid_roi_found = True
			pad = "{:.4f}".format(0.0)
			label_idx = "{:.4f}".format(0.0)
			xmin = "{:.4f}".format(float(anno_arr[0])/img_width_real)
			ymin = "{:.4f}".format(float(anno_arr[1])/img_height_real)
			xmax = "{:.4f}".format(float(anno_arr[2])/img_width_real)
			ymax = "{:.4f}".format(float(anno_arr[3])/img_height_real)
			label_content_str += label_idx + '\t' + xmin +  '\t' + ymin + '\t'\
										 				+ xmax + '\t' + ymax + '\t' + pad + '\t'	
							
		if valid_roi_found:
			label_content_str += img_path_to_record + '\n'
			output_file.write(label_content_str)

	output_file.close()