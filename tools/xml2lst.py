# -*- coding: utf-8 -*-

import os, sys, argparse, fnmatch, xml.etree.cElementTree as ET
import cv2

#LABELS = ['FaPiao_DaiMa', 'FaPiao_HaoMa', 'JinE_1']
#LABELS = ['FaPiao_DaiMa', 'FaPiao_HaoMa', 'ShouKuan_DanWei', 'RiQi', 'JinE_XiaoXie', 'JinE_1']
#LABELS = ['Text']
#LABELS = ['BianHao']
#LABELS = ['BianHao', 'RiQi', 'YiYuan', 'JinE_1']
#LABELS = ['YinZhang', 'ShouJu_MenZhen', 'ShouJu_ZhuYuan', 'QiTa_C']
LABELS = ['X_PlateN_1', 'X_Owner', 'X_VIN', 'X_XingZhi', 'X_Model']

if __name__ == "__main__":
	reload(sys)
	sys.setdefaultencoding("utf-8")

	parser = argparse.ArgumentParser()
	parser.add_argument("--src")
	parser.add_argument("--txt")
	args = parser.parse_args()
	SRC_DIR = args.src
	TXT_FILE = args.txt

	txt_file = open(TXT_FILE, 'r')
	output_file_name = os.path.join(SRC_DIR, os.path.split(TXT_FILE)[1].split('.')[0] + '.lst')
	if os.path.exists(output_file_name):
		os.remove(output_file_name)
	output_file = open(output_file_name, 'w')

	annotations = txt_file.readlines()

	anno_ctr = 0
	for anno in annotations:
		anno_str = str(anno_ctr)
		anno_str += '\t2\t6\t'
		imgNxml = anno.split('\t')
		img_path_to_record = imgNxml[0]
		xml_path = imgNxml[1][:-1]	#os.path.join(SRC_DIR, imgNxml[1][:-1])
		tree = ET.ElementTree(file=xml_path)
		tree_root = tree.getroot()
		sizes = tree_root.findall('size')
		img_width = int(sizes[0].find('width').text)
		img_height = int(sizes[0].find('height').text)
		img_path_full = img_path_to_record	#os.path.join(SRC_DIR, img_path_to_record)

		img_data = cv2.imread(img_path_full)
		img_width_real = img_data.shape[1]
		img_height_real = img_data.shape[0]
		if img_width_real != img_width or img_height_real != img_height:
			print(img_path_full)
			continue

		objs = tree_root.findall('object')

		label_content_str = anno_str
		valid_roi_found = False
		for obj in objs:
			label_name = obj.find('name').text
			if label_name in LABELS:
				valid_roi_found = True
				label_idx = "{:.4f}".format(float(LABELS.index(label_name)))
				pad = "{:.4f}".format(0.0)
				bndbox = obj.find('bndbox')
				xmin = "{:.4f}".format(float(bndbox.find('xmin').text)/img_width)
				ymin = "{:.4f}".format(float(bndbox.find('ymin').text)/img_height)
				xmax = "{:.4f}".format(float(bndbox.find('xmax').text)/img_width)
				ymax = "{:.4f}".format(float(bndbox.find('ymax').text)/img_height)
				label_content_str += label_idx + '\t' + xmin +  '\t' + ymin + '\t'\
										 				+ xmax + '\t' + ymax + '\t' + pad + '\t'							 				
			else:
				continue
		if valid_roi_found:
			label_content_str += img_path_to_record + '\n'
			output_file.write(label_content_str)

	output_file.close()