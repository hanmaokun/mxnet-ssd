import sys, random, os
sys.path.insert(0, '../../python')
import mxnet as mx
import numpy as np
import triplet_loss, cv2
from operator import itemgetter
import random
import fnmatch
import matplotlib.pyplot as plt

random.seed(200)
_, arg_params, __ = mx.model.load_checkpoint('/home/nlp/bigsur/devel/ocr/mxnet-ssd/img_comp', 22)

batch_size = 1
network = triplet_loss.get_sim_net()

input_shapes = dict([('same', (batch_size, 3, 128, 64)),\
                     ('diff', (batch_size, 3, 128, 64))])
executor = network.simple_bind(ctx = mx.gpu(), **input_shapes)
for key in executor.arg_dict.keys():
    if key in arg_params:
        print key, arg_params[key].shape, executor.arg_dict[key].shape
        arg_params[key].copyto(executor.arg_dict[key])

def softmax(x):
    x=x.astype(float)
    if x.ndim==1:
        S=np.sum(np.exp(x))
        return np.exp(x)/S
    elif x.ndim==2:
        result=np.zeros_like(x)
        M,N=x.shape
        for n in range(N):
            S=np.sum(np.exp(x[:,n]))
            result[:,n]=np.exp(x[:,n])/S
        return result
    else:
        print("The input array is not 1- or 2-dimensional.")

def load_img(img_path):
    img_data = cv2.imread(img_path)
    img_data = cv2.resize(img_data, (128, 64))
    img_data = np.asarray(img_data)
    img_data = img_data.transpose(2, 1, 0)
    return img_data

real_src_dir = '/home/nlp/bigsur/data/chanxian/img_comp_data/val/A'
fake_src_dir = '/home/nlp/bigsur/data/chanxian/img_comp_data/val/B' 

file_names = []
for root, dir_names, file_names in os.walk(real_src_dir):    
    for file_name in fnmatch.filter(file_names, '*.jpg'):
        file_names.append(file_name)

dists_real = []
dists_fake = []

for root, dir_names, file_names in os.walk(real_src_dir):    
    for file_name in fnmatch.filter(file_names, '*.jpg'):
        real_file_path = os.path.join(root, file_name)
        fake_file_path = os.path.join(fake_src_dir, file_name)

        fn_real = load_img(real_file_path)
        fn_fake = load_img(fake_file_path)

        outputs = executor.forward(is_train = True, same = mx.nd.array([fn_real]),
                         diff = mx.nd.array([fn_fake]))
        dis_real = outputs[0].asnumpy()[0]

        dis_fakes = []
        for rnd_count in xrange(1):
            fake_img_name = file_name
            while fake_img_name == file_name:
                RDM = random.randint(1, len(file_names)-1)
                fake_img_name = file_names[RDM]
            random_fake_file_path = os.path.join(fake_src_dir, fake_img_name)
            fn_random = load_img(random_fake_file_path)

            outputs = executor.forward(is_train = True, same = mx.nd.array([fn_real]),
                             diff = mx.nd.array([fn_random]))
            dis_fake = outputs[0].asnumpy()[0]
            dis_fakes.append(dis_fake)

        dists_real.append(dis_real)
        dists_fake.append(reduce(lambda x, y: x + y, dis_fakes) / len(dis_fakes))

        #print(str(dis_real) + '\t' + str(dis_fake))

dists_real_mean = reduce(lambda x, y: x + y, dists_real) / len(dists_real)
dists_fake_mean = reduce(lambda x, y: x + y, dists_fake) / len(dists_fake)

print(str(dists_real_mean) + ' - ' + str(dists_fake_mean))

thres = (dists_real_mean + dists_fake_mean)/2
thres = 0.85
miss_det = 0
false_det = 0
sample_len = len(dists_real)
abnormal_ctr = 0
norm_results = []

# for i in xrange(sample_len):
#     dists_real[i] = np.exp(dists_real[i])
#     dists_fake[i] = np.exp(dists_fake[i])

for i in xrange(sample_len):
    real_fake_result_pair = []
    if dists_real[i] > dists_fake[i]:
        abnormal_ctr += 1

    if dists_real[i] >= thres:
        miss_det += 1

    if dists_fake[i] < thres:
        false_det += 1

    real_fake_result_pair.append(dists_real[i])
    real_fake_result_pair.append(dists_fake[i])
    norm_results.append(softmax(np.array(real_fake_result_pair)))

#print(norm_results)

norm_failed_ctr = 0
for i in xrange(len(norm_results)):
    norm_real, norm_fake = norm_results[i]
    if norm_real >= norm_fake:
        norm_failed_ctr += 1

print(str(norm_failed_ctr) + ' in ' + str(sample_len) + ' images.')
print(str(abnormal_ctr) + ' in ' + str(sample_len) + ' images.')
print('miss detection: ' + str(miss_det) + ', false detection: ' + str(false_det))

plt.plot(dists_real)
plt.plot(dists_fake)
plt.ylabel('distances')
plt.show()

exit(0)

###############################################################################
img_same_path = '/home/nlp/bigsur/data/chanxian/test_compare/1_real.jpg'
img_same = load_img(img_same_path)
img_diff_path = '/home/nlp/bigsur/data/chanxian/test_compare/random_fake.jpg'
img_diff = load_img(img_diff_path)

outputs = executor.forward(is_train = True, same = mx.nd.array([img_same]),
                 diff = mx.nd.array([img_diff]))
dis = outputs[0].asnumpy()[0]
print(dis)

exit(0)

################################################################################
root = sys.argv[1]
names = []
for fn in os.listdir(root):
    if fn.endswith('.npy'):
        names.append(root + '/' + fn)
random.shuffle(names)

imgs = []
for i in range(10):
    imgs.append(np.load(names[i]))

def save_img(fname, im):
    a = np.copy(im) * 255.0
    cv2.imwrite(fname, a.transpose(1, 2, 0))

src = imgs[0][random.randint(0, len(imgs[0]) - 1)]
save_img("src.png", src)
dsts = []
for i in range(10):
    for j in range(128):
        k = random.randint(0, len(imgs[i]) - 1)
        dst = imgs[i][k]
        outputs = executor.forward(is_train = True, same = mx.nd.array([src]),
                         diff = mx.nd.array([dst]))
        dis = outputs[0].asnumpy()[0]
        dsts.append((dst, dis, i))
        
i = 0
for img, w, la in sorted(dsts, key = itemgetter(1))[:10]:
    print w, la
    save_img("dst_" + str(i) + ".png", img)
    i += 1

