import os, sys

class Config:
    def __init__(self):
        return

	MEAN=np.float32([159.4, 170.6, 175.9])

    classes=['BianHao','RiQi','YiYuan','JinE_1']

    TEST_GPU_ID=0
    NUM_CLASSES=5
    SCALE=896
    MAX_SCALE=896

    LINE_MIN_SCORE = 0.3
    TEXT_PROPOSALS_MIN_SCORE = 0.5
    TEXT_PROPOSALS_NMS_THRESH = 0.3
    MAX_HORIZONTAL_GAP = 50
    TEXT_LINE_NMS_THRESH = 0.3
    MIN_NUM_PROPOSALS = 2
    MIN_RATIO = 1.2
    MIN_V_OVERLAPS = 0.7
    MIN_SIZE_SIM = 0.7
    TEXT_PROPOSALS_WIDTH = 16
    NUM_SELECTED = 60
    
    NUM_TEST_IMAGES = 651
    
    ROOT_DATA_DIR = os.getenv('DATA_DIR')
    IMG_EXTENTION = ['jpg']
    SEED = 100
